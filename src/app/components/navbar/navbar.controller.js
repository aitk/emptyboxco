'use strict';

angular.module('getEmptyBoxesApp')
  .controller('NavbarCtrl', function (config, $scope, $location) {
    $scope.pageName = config.pageName;
    $scope.links = [];

    //Check which is active.
    console.log($location.path());

    $scope.isLoggedIn = false;
  });
