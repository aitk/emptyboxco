'use strict';
angular.module('getEmptyBoxesApp', [ 'ngResource', 'ngRoute', 'chart.js'])
  .config(function ($routeProvider, $locationProvider, ChartJsProvider) {

    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      }).when('/about', {
        templateUrl: 'app/about/about.html',
        controller: 'AboutCtrl'
      }).when('/contact',{
        templateUrl:'app/contact/contact.html',
        controller: 'ContactCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
  })
;
