# angular-webapp-template

## Getting started

To setup the project download the zip or clone the repo using 
```git clone https://github.com/ChrisAitken/angular-webapp-template.git``` 

After the project has downloaded you will need to install the dependencies with npm and bower by using the command 
```npm install && bower install```

Once the all the dependencies are completed all that is required is to run 
```gulp serve``` to view the page which will open at [http://localhost:3000](http://localhost:3000)

